/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject;

import javax.persistence.*;

/**
 *
 * @author Rooben
 */


@Entity
@Table(name="Parkovsite")
public class Parking {
    
    @Id
    @Column(name = "kod_parkoviste")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "ulice")
    private String street;
    
    @Column(name = "cislo_popisne")
    private int addressNumber;
    
    @Column(name = "mesto")
    private String city;
    
    @Column(name = "psc")
    private int postalNumber;
    
    @Column(name = "zemepisna_sirka")
    private String latitude;
    
    @Column(name = "zemepisna_delka")
    private String longitude;
    
    @Column(name = "otevreno_od")
    private String  openTime ;
    
    @Column(name = "otevreno_do")
    private String  closeTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(int addressNumber) {
        this.addressNumber = addressNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPostalNumber() {
        return postalNumber;
    }

    public void setPostalNumber(int postalNumber) {
        this.postalNumber = postalNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public String toString() {
        return "Parking{" 
                + "id=" + id 
                + ", street=" + street 
                + ", addressNumber=" + addressNumber 
                + ", city=" + city 
                + ", postalNumber=" + postalNumber 
                + ", latitude=" + latitude
                + ", longitude=" + longitude 
                + ", openTime=" + openTime 
                + ", closeTime=" + closeTime + '}';
    }

   
}
