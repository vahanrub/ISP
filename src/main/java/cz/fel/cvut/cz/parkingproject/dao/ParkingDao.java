/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject.dao;

import cz.fel.cvut.cz.parkingproject.Parking;
import java.util.List;

/**
 *
 * @author Rooben
 */
public interface ParkingDao {

    public void addParking(Parking parking);

    public void upadateParking(Parking parking);

    public void deleteParking(int id);

    public Parking getParkingById(int id);

    public List<Parking> getAllParking();
}
