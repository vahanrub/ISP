/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.prakingnew;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Rooben
 */
@Entity
@Table(name = "typ_vozidla", catalog = "db17_vahanrub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "TypVozidla.findAll", query = "SELECT t FROM TypVozidla t")
    , @NamedQuery(name = "TypVozidla.findByTypVozidla", query = "SELECT t FROM TypVozidla t WHERE t.typVozidla = :typVozidla")
    , @NamedQuery(name = "TypVozidla.findByNazev", query = "SELECT t FROM TypVozidla t WHERE t.nazev = :nazev")})
public class TypVozidla implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "typ_vozidla")
    private Integer typVozidla;
    @Basic(optional = false)
    @Column(name = "nazev")
    private String nazev;

    public TypVozidla() {
    }

    public TypVozidla(Integer typVozidla) {
        this.typVozidla = typVozidla;
    }

    public TypVozidla(Integer typVozidla, String nazev) {
        this.typVozidla = typVozidla;
        this.nazev = nazev;
    }

    public Integer getTypVozidla() {
        return typVozidla;
    }

    public void setTypVozidla(Integer typVozidla) {
        Integer oldTypVozidla = this.typVozidla;
        this.typVozidla = typVozidla;
        changeSupport.firePropertyChange("typVozidla", oldTypVozidla, typVozidla);
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        String oldNazev = this.nazev;
        this.nazev = nazev;
        changeSupport.firePropertyChange("nazev", oldNazev, nazev);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typVozidla != null ? typVozidla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypVozidla)) {
            return false;
        }
        TypVozidla other = (TypVozidla) object;
        if ((this.typVozidla == null && other.typVozidla != null) || (this.typVozidla != null && !this.typVozidla.equals(other.typVozidla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nazev;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
