/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.prakingnew;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Rooben
 */
@Entity
@Table(name = "typ_parkoviste", catalog = "db17_vahanrub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "TypParkoviste.findAll", query = "SELECT t FROM TypParkoviste t")
    , @NamedQuery(name = "TypParkoviste.findByTypParkoviste", query = "SELECT t FROM TypParkoviste t WHERE t.typParkoviste = :typParkoviste")
    , @NamedQuery(name = "TypParkoviste.findByNazev", query = "SELECT t FROM TypParkoviste t WHERE t.nazev = :nazev")})
public class TypParkoviste implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "typ_parkoviste")
    private Integer typParkoviste;
    @Basic(optional = false)
    @Column(name = "nazev")
    private String nazev;

    public TypParkoviste() {
    }

    public TypParkoviste(Integer typParkoviste) {
        this.typParkoviste = typParkoviste;
    }

    public TypParkoviste(Integer typParkoviste, String nazev) {
        this.typParkoviste = typParkoviste;
        this.nazev = nazev;
    }

    public Integer getTypParkoviste() {
        return typParkoviste;
    }

    public void setTypParkoviste(Integer typParkoviste) {
        Integer oldTypParkoviste = this.typParkoviste;
        this.typParkoviste = typParkoviste;
        changeSupport.firePropertyChange("typParkoviste", oldTypParkoviste, typParkoviste);
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        String oldNazev = this.nazev;
        this.nazev = nazev;
        changeSupport.firePropertyChange("nazev", oldNazev, nazev);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typParkoviste != null ? typParkoviste.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypParkoviste)) {
            return false;
        }
        TypParkoviste other = (TypParkoviste) object;
        if ((this.typParkoviste == null && other.typParkoviste != null) || (this.typParkoviste != null && !this.typParkoviste.equals(other.typParkoviste))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.fel.cvut.cz.prakingnew.TypParkoviste[ typParkoviste=" + typParkoviste + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
