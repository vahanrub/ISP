/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject.dao;

import cz.fel.cvut.cz.parkingproject.Parking;
import cz.fel.cvut.cz.parkingproject.ParkingAutos;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rooben
 */
public class ParkingAutosDaoImp implements ParkingAutosDao{
    
     private static final Logger logger = LoggerFactory.getLogger(ParkingAutosDaoImp.class);
     
   EntityManagerFactory emf = Persistence.createEntityManagerFactory("ParkingNew");
   EntityManager em = emf.createEntityManager();
   EntityTransaction tx = em.getTransaction();

    @Override
    public void addParkingAuto(ParkingAutos parkingAuto) {
        tx.begin();
        em.persist(parkingAuto);
        tx.commit();
        logger.info("Parking successfully saved. Parking details: "+ parkingAuto);
    }

    @Override
    public void upadateParkingAuto(ParkingAutos parkingAuto) {
      final ParkingAutos find = em.find(ParkingAutos.class, parkingAuto); 
        if(find != null){
           em.merge(parkingAuto);
        }        
        logger.info("Parking successfully updated. Parking details: "+ parkingAuto);
    }

    @Override
    public void deleteParkingAuto(int id) {
        final ParkingAutos find = em.find(ParkingAutos.class, id);
        if(find != null){
            em.remove(find);
        }
        logger.info("Parking successfully removed. Parking details: "+ find);
    }

    @Override
    public ParkingAutos getParkingAutoById(int id) {
        final ParkingAutos find = em.find(ParkingAutos.class, id);
        logger.info("Parking successfully loaded. Parking details: "+ find);  
        return find;
    }

    @Override
    public List<ParkingAutos> getAllParkingAutos() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ParkingAutos> cq = cb.createQuery(ParkingAutos.class);
        Root<ParkingAutos> rootEntry = cq.from(ParkingAutos.class);
        CriteriaQuery<ParkingAutos> all = cq.select(rootEntry);
        TypedQuery<ParkingAutos> allQuery = em.createQuery(all);
        return allQuery.getResultList();
    }

   
}
