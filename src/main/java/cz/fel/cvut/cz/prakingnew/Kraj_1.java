/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.prakingnew;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Rooben
 */
@Entity
@Table(name = "kraj", catalog = "db17_vahanrub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Kraj_1.findAll", query = "SELECT k FROM Kraj_1 k")
    , @NamedQuery(name = "Kraj_1.findByIdKraje", query = "SELECT k FROM Kraj_1 k WHERE k.idKraje = :idKraje")
    , @NamedQuery(name = "Kraj_1.findByNazev", query = "SELECT k FROM Kraj_1 k WHERE k.nazev = :nazev")})
public class Kraj_1 implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_kraje")
    private Integer idKraje;
    @Basic(optional = false)
    @Column(name = "nazev")
    private String nazev;

    public Kraj_1() {
    }

    public Kraj_1(Integer idKraje) {
        this.idKraje = idKraje;
    }

    public Kraj_1(Integer idKraje, String nazev) {
        this.idKraje = idKraje;
        this.nazev = nazev;
    }

    public Integer getIdKraje() {
        return idKraje;
    }

    public void setIdKraje(Integer idKraje) {
        Integer oldIdKraje = this.idKraje;
        this.idKraje = idKraje;
        changeSupport.firePropertyChange("idKraje", oldIdKraje, idKraje);
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        String oldNazev = this.nazev;
        this.nazev = nazev;
        changeSupport.firePropertyChange("nazev", oldNazev, nazev);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKraje != null ? idKraje.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kraj_1)) {
            return false;
        }
        Kraj_1 other = (Kraj_1) object;
        if ((this.idKraje == null && other.idKraje != null) || (this.idKraje != null && !this.idKraje.equals(other.idKraje))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nazev;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
