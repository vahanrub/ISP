/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject.dao;

import cz.fel.cvut.cz.parkingproject.Parking;
import cz.fel.cvut.cz.parkingproject.ParkingAutos;
import java.util.List;

/**
 *
 * @author Rooben
 */
public interface ParkingAutosDao {
    
    public void addParkingAuto(ParkingAutos parkingAuto);

    public void upadateParkingAuto(ParkingAutos parkingAuto);

    public void deleteParkingAuto(int id);

    public ParkingAutos getParkingAutoById(int id);

    public List<ParkingAutos> getAllParkingAutos();
}
