/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject.dao;

import cz.fel.cvut.cz.parkingproject.ParkingAutos;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rooben
 */
public class ParkingAutosDaoImpTest {
    
    
     /**
     * Test of getAllParkingAutos method, of class ParkingAutosDaoImp.
     */
    @Test
    public void testGetAllParkingAutos() {
        ParkingAutosDaoImp instance = new ParkingAutosDaoImp();
        ParkingAutos pa = new ParkingAutos();
        pa.setParkingFee(150.0);
        pa.setArrival("11/11/2016");
        pa.setDeparture("11/12/2016");
        pa.setSpz("456SS456");       
        instance.addParkingAuto(pa);
        List<ParkingAutos> result = instance.getAllParkingAutos();
        ParkingAutos get = result.get(0);
        assertEquals(pa, get);
    }
        public ParkingAutosDaoImpTest() {
    }
    
    
    
     /**
     * Test of getParkingAutoById method, of class ParkingAutosDaoImp.
     */
    @Test
    public void testGetParkingAutoById() {

            ParkingAutosDaoImp instance = new ParkingAutosDaoImp();
        ParkingAutos pa = new ParkingAutos();
        int id = 0;
           pa.setParkingFee(150.0);
        pa.setArrival("11/11/2016");
        pa.setDeparture("11/12/2016");
        pa.setSpz("456SS456");  
        ParkingAutos expResult = null;
        ParkingAutos result = instance.getParkingAutoById(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }


    /**
     * Test of addParkingAuto method, of class ParkingAutosDaoImp.
     */
    @Test
    public void testAddParkingAuto() {
        System.out.println("addParkingAuto");
        ParkingAutos parkingAuto = null;
        ParkingAutosDaoImp instance = new ParkingAutosDaoImp();
        instance.addParkingAuto(parkingAuto);
        fail("The test case is a prototype.");
    }

    /**
     * Test of upadateParkingAuto method, of class ParkingAutosDaoImp.
     */
    @Test
    public void testUpadateParkingAuto() {
        System.out.println("upadateParkingAuto");
        ParkingAutos parkingAuto = null;
        ParkingAutosDaoImp instance = new ParkingAutosDaoImp();
        instance.upadateParkingAuto(parkingAuto);
    }

    /**
     * Test of deleteParkingAuto method, of class ParkingAutosDaoImp.
     */
    @Test
    public void testDeleteParkingAuto() {
        System.out.println("deleteParkingAuto");
        int id = 0;
        ParkingAutosDaoImp instance = new ParkingAutosDaoImp();
        instance.deleteParkingAuto(id);
        fail("The test case is a prototype.");
    }



    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

}
