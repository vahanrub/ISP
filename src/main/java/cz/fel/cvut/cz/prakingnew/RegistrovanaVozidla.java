/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.prakingnew;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Rooben
 */
@Entity
@Table(name = "registrovana_vozidla", catalog = "db17_vahanrub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "RegistrovanaVozidla.findAll", query = "SELECT r FROM RegistrovanaVozidla r")
    , @NamedQuery(name = "RegistrovanaVozidla.findBySpz", query = "SELECT r FROM RegistrovanaVozidla r WHERE r.spz = :spz")
    , @NamedQuery(name = "RegistrovanaVozidla.findByDatumRegistrace", query = "SELECT r FROM RegistrovanaVozidla r WHERE r.datumRegistrace = :datumRegistrace")
    , @NamedQuery(name = "RegistrovanaVozidla.findByPrijmeni", query = "SELECT r FROM RegistrovanaVozidla r WHERE r.prijmeni = :prijmeni")
    , @NamedQuery(name = "RegistrovanaVozidla.findByJmeno", query = "SELECT r FROM RegistrovanaVozidla r WHERE r.jmeno = :jmeno")
    , @NamedQuery(name = "RegistrovanaVozidla.findByEmail", query = "SELECT r FROM RegistrovanaVozidla r WHERE r.email = :email")
    , @NamedQuery(name = "RegistrovanaVozidla.findByMobil", query = "SELECT r FROM RegistrovanaVozidla r WHERE r.mobil = :mobil")
    , @NamedQuery(name = "RegistrovanaVozidla.findByTypVozidla", query = "SELECT r FROM RegistrovanaVozidla r WHERE r.typVozidla = :typVozidla")})
public class RegistrovanaVozidla implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "spz")
    private String spz;
    @Basic(optional = false)
    @Column(name = "datum_registrace")
    @Temporal(TemporalType.DATE)
    private Date datumRegistrace;
    @Basic(optional = false)
    @Column(name = "prijmeni")
    private String prijmeni;
    @Basic(optional = false)
    @Column(name = "jmeno")
    private String jmeno;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "mobil")
    private int mobil;
    @Basic(optional = false)
    @Column(name = "typ_vozidla")
    private int typVozidla;

    public RegistrovanaVozidla() {
    }

    public RegistrovanaVozidla(String spz) {
        this.spz = spz;
    }

    public RegistrovanaVozidla(String spz, Date datumRegistrace, String prijmeni, String jmeno, String email, int mobil, int typVozidla) {
        this.spz = spz;
        this.datumRegistrace = datumRegistrace;
        this.prijmeni = prijmeni;
        this.jmeno = jmeno;
        this.email = email;
        this.mobil = mobil;
        this.typVozidla = typVozidla;
    }

    public String getSpz() {
        return spz;
    }

    public void setSpz(String spz) {
        String oldSpz = this.spz;
        this.spz = spz;
        changeSupport.firePropertyChange("spz", oldSpz, spz);
    }

    public Date getDatumRegistrace() {
        return datumRegistrace;
    }

    public void setDatumRegistrace(Date datumRegistrace) {
        Date oldDatumRegistrace = this.datumRegistrace;
        this.datumRegistrace = datumRegistrace;
        changeSupport.firePropertyChange("datumRegistrace", oldDatumRegistrace, datumRegistrace);
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        String oldPrijmeni = this.prijmeni;
        this.prijmeni = prijmeni;
        changeSupport.firePropertyChange("prijmeni", oldPrijmeni, prijmeni);
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        String oldJmeno = this.jmeno;
        this.jmeno = jmeno;
        changeSupport.firePropertyChange("jmeno", oldJmeno, jmeno);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        String oldEmail = this.email;
        this.email = email;
        changeSupport.firePropertyChange("email", oldEmail, email);
    }

    public int getMobil() {
        return mobil;
    }

    public void setMobil(int mobil) {
        int oldMobil = this.mobil;
        this.mobil = mobil;
        changeSupport.firePropertyChange("mobil", oldMobil, mobil);
    }

    public int getTypVozidla() {
        return typVozidla;
    }

    public void setTypVozidla(int typVozidla) {
        int oldTypVozidla = this.typVozidla;
        this.typVozidla = typVozidla;
        changeSupport.firePropertyChange("typVozidla", oldTypVozidla, typVozidla);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (spz != null ? spz.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistrovanaVozidla)) {
            return false;
        }
        RegistrovanaVozidla other = (RegistrovanaVozidla) object;
        if ((this.spz == null && other.spz != null) || (this.spz != null && !this.spz.equals(other.spz))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.fel.cvut.cz.prakingnew.RegistrovanaVozidla[ spz=" + spz + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
