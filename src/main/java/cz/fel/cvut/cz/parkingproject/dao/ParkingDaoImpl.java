/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject.dao;

import cz.fel.cvut.cz.parkingproject.Parking;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rooben
 */


public class ParkingDaoImpl implements ParkingDao {

    private static final Logger logger = LoggerFactory.getLogger(ParkingDaoImpl.class);
    
   EntityManagerFactory emf = Persistence.createEntityManagerFactory("ParkingProject");
   EntityManager em = emf.createEntityManager();
   EntityTransaction tx = em.getTransaction();
  
    @Override
    public void addParking(Parking parking) {
        tx.begin();
        em.persist(parking);
        tx.commit();
        logger.info("Parking successfully saved. Parking details: "+ parking);
    }

    @Override
    public void upadateParking(Parking parking) {
        final Parking find = em.find(Parking.class, parking); 
        if(find != null){
           em.merge(parking);
        }        
        logger.info("Parking successfully updated. Parking details: "+ parking);
    }

    @Override
    public void deleteParking(int id) {
        final Parking find = em.find(Parking.class, id);
        if(find != null){
            em.remove(find);
        }
        logger.info("Parking successfully removed. Parking details: "+ find);
    }

    @Override
    public Parking getParkingById(int id) {
         final Parking find = em.find(Parking.class, id);
        logger.info("Parking successfully loaded. Parking details: "+ find);  
        return find;
    }

    @Override
    public List<Parking> getAllParking() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Parking> cq = cb.createQuery(Parking.class);
        Root<Parking> rootEntry = cq.from(Parking.class);
        CriteriaQuery<Parking> all = cq.select(rootEntry);
        TypedQuery<Parking> allQuery = em.createQuery(all);
        return allQuery.getResultList();
           
    }
    
}
