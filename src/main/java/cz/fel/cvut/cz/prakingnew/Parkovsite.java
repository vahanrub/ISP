/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.prakingnew;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Rooben
 */
@Entity
@Table(name = "parkovsite", catalog = "db17_vahanrub", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Parkovsite.findAll", query = "SELECT p FROM Parkovsite p")
    , @NamedQuery(name = "Parkovsite.findByKodParkoviste", query = "SELECT p FROM Parkovsite p WHERE p.kodParkoviste = :kodParkoviste")
    , @NamedQuery(name = "Parkovsite.findByUlice", query = "SELECT p FROM Parkovsite p WHERE p.ulice = :ulice")
    , @NamedQuery(name = "Parkovsite.findByCisloPopisne", query = "SELECT p FROM Parkovsite p WHERE p.cisloPopisne = :cisloPopisne")
    , @NamedQuery(name = "Parkovsite.findByMesto", query = "SELECT p FROM Parkovsite p WHERE p.mesto = :mesto")
    , @NamedQuery(name = "Parkovsite.findByPsc", query = "SELECT p FROM Parkovsite p WHERE p.psc = :psc")
    , @NamedQuery(name = "Parkovsite.findByZemepisnaSirka", query = "SELECT p FROM Parkovsite p WHERE p.zemepisnaSirka = :zemepisnaSirka")
    , @NamedQuery(name = "Parkovsite.findByZemepisnaDelka", query = "SELECT p FROM Parkovsite p WHERE p.zemepisnaDelka = :zemepisnaDelka")
    , @NamedQuery(name = "Parkovsite.findByOtevrenoOd", query = "SELECT p FROM Parkovsite p WHERE p.otevrenoOd = :otevrenoOd")
    , @NamedQuery(name = "Parkovsite.findByOtevrenoDo", query = "SELECT p FROM Parkovsite p WHERE p.otevrenoDo = :otevrenoDo")
    , @NamedQuery(name = "Parkovsite.findByIdKraje", query = "SELECT p FROM Parkovsite p WHERE p.idKraje = :idKraje")})
public class Parkovsite implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "kod_parkoviste")
    private Long kodParkoviste;
    @Basic(optional = false)
    @Column(name = "ulice")
    private String ulice;
    @Basic(optional = false)
    @Column(name = "cislo_popisne")
    private int cisloPopisne;
    @Basic(optional = false)
    @Column(name = "mesto")
    private String mesto;
    @Basic(optional = false)
    @Column(name = "psc")
    private int psc;
    @Basic(optional = false)
    @Column(name = "zemepisna_sirka")
    private String zemepisnaSirka;
    @Basic(optional = false)
    @Column(name = "zemepisna_delka")
    private String zemepisnaDelka;
    @Basic(optional = false)
    @Column(name = "otevreno_od")
    @Temporal(TemporalType.DATE)
    private Date otevrenoOd;
    @Basic(optional = false)
    @Column(name = "otevreno_do")
    @Temporal(TemporalType.DATE)
    private Date otevrenoDo;
    @Column(name = "id_kraje")
    private Integer idKraje;

    public Parkovsite() {
    }

    public Parkovsite(Long kodParkoviste) {
        this.kodParkoviste = kodParkoviste;
    }

    public Parkovsite(Long kodParkoviste, String ulice, int cisloPopisne, String mesto, int psc, String zemepisnaSirka, String zemepisnaDelka, Date otevrenoOd, Date otevrenoDo) {
        this.kodParkoviste = kodParkoviste;
        this.ulice = ulice;
        this.cisloPopisne = cisloPopisne;
        this.mesto = mesto;
        this.psc = psc;
        this.zemepisnaSirka = zemepisnaSirka;
        this.zemepisnaDelka = zemepisnaDelka;
        this.otevrenoOd = otevrenoOd;
        this.otevrenoDo = otevrenoDo;
    }

    public Long getKodParkoviste() {
        return kodParkoviste;
    }

    public void setKodParkoviste(Long kodParkoviste) {
        Long oldKodParkoviste = this.kodParkoviste;
        this.kodParkoviste = kodParkoviste;
        changeSupport.firePropertyChange("kodParkoviste", oldKodParkoviste, kodParkoviste);
    }

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        String oldUlice = this.ulice;
        this.ulice = ulice;
        changeSupport.firePropertyChange("ulice", oldUlice, ulice);
    }

    public int getCisloPopisne() {
        return cisloPopisne;
    }

    public void setCisloPopisne(int cisloPopisne) {
        int oldCisloPopisne = this.cisloPopisne;
        this.cisloPopisne = cisloPopisne;
        changeSupport.firePropertyChange("cisloPopisne", oldCisloPopisne, cisloPopisne);
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        String oldMesto = this.mesto;
        this.mesto = mesto;
        changeSupport.firePropertyChange("mesto", oldMesto, mesto);
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        int oldPsc = this.psc;
        this.psc = psc;
        changeSupport.firePropertyChange("psc", oldPsc, psc);
    }

    public String getZemepisnaSirka() {
        return zemepisnaSirka;
    }

    public void setZemepisnaSirka(String zemepisnaSirka) {
        String oldZemepisnaSirka = this.zemepisnaSirka;
        this.zemepisnaSirka = zemepisnaSirka;
        changeSupport.firePropertyChange("zemepisnaSirka", oldZemepisnaSirka, zemepisnaSirka);
    }

    public String getZemepisnaDelka() {
        return zemepisnaDelka;
    }

    public void setZemepisnaDelka(String zemepisnaDelka) {
        String oldZemepisnaDelka = this.zemepisnaDelka;
        this.zemepisnaDelka = zemepisnaDelka;
        changeSupport.firePropertyChange("zemepisnaDelka", oldZemepisnaDelka, zemepisnaDelka);
    }

    public Date getOtevrenoOd() {
        return otevrenoOd;
    }

    public void setOtevrenoOd(Date otevrenoOd) {
        Date oldOtevrenoOd = this.otevrenoOd;
        this.otevrenoOd = otevrenoOd;
        changeSupport.firePropertyChange("otevrenoOd", oldOtevrenoOd, otevrenoOd);
    }

    public Date getOtevrenoDo() {
        return otevrenoDo;
    }

    public void setOtevrenoDo(Date otevrenoDo) {
        Date oldOtevrenoDo = this.otevrenoDo;
        this.otevrenoDo = otevrenoDo;
        changeSupport.firePropertyChange("otevrenoDo", oldOtevrenoDo, otevrenoDo);
    }

    public Integer getIdKraje() {
        return idKraje;
    }

    public void setIdKraje(Integer idKraje) {
        Integer oldIdKraje = this.idKraje;
        this.idKraje = idKraje;
        changeSupport.firePropertyChange("idKraje", oldIdKraje, idKraje);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kodParkoviste != null ? kodParkoviste.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parkovsite)) {
            return false;
        }
        Parkovsite other = (Parkovsite) object;
        if ((this.kodParkoviste == null && other.kodParkoviste != null) || (this.kodParkoviste != null && !this.kodParkoviste.equals(other.kodParkoviste))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.fel.cvut.cz.prakingnew.Parkovsite[ kodParkoviste=" + kodParkoviste + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
