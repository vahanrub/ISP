/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject.dao;

import cz.fel.cvut.cz.parkingproject.ParkingAutos;
import cz.fel.cvut.cz.parkingproject.RegistereAuto;
import java.util.List;

/**
 *
 * @author Rooben
 */
public interface RegistereAutoDao {
    
    public void addRegistereAuto(RegistereAuto registereAuto);

    public void upadateRegistereAuto(RegistereAuto registereAuto);

    public void deleteRegistereAuto(int id);

    public RegistereAuto getRegistereAutoById(int id);

    public List<RegistereAuto> getAllRegistereAuto();
}
