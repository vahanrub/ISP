/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject.dao;

import cz.fel.cvut.cz.parkingproject.Parking;
import cz.fel.cvut.cz.parkingproject.ParkingAutos;
import cz.fel.cvut.cz.parkingproject.RegistereAuto;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Rooben
 */
public class RegistereAutoDaoImp implements RegistereAutoDao{
    
    
    private static final Logger logger = LoggerFactory.getLogger(RegistereAutoDaoImp.class);
    
   EntityManagerFactory emf = Persistence.createEntityManagerFactory("ParkingProject");
   EntityManager em = emf.createEntityManager();
   EntityTransaction tx = em.getTransaction();

    @Override
    public void addRegistereAuto(RegistereAuto registereAuto) {
        tx.begin();
        em.persist(registereAuto);
        tx.commit();
        logger.info("registereAuto successfully saved. Parking details: "+ registereAuto);
    }

    @Override
    public void upadateRegistereAuto(RegistereAuto registereAuto) {
        final RegistereAuto find = em.find(RegistereAuto.class, registereAuto); 
        if(find != null){
           em.merge(registereAuto);
        }        
        logger.info("registereAuto successfully updated. Parking details: "+ registereAuto);
    }

    @Override
    public void deleteRegistereAuto(int id) {
        final RegistereAuto find = em.find(RegistereAuto.class, id);
        if(find != null){
            em.remove(find);
        }
        logger.info("RegistereAuto successfully removed. Parking details: "+ find);
    }

    @Override
    public RegistereAuto getRegistereAutoById(int id) {
         final RegistereAuto find = em.find(RegistereAuto.class, id);
        logger.info("RegistereAuto successfully loaded. Parking details: "+ find);  
        return find;
    }

    @Override
    public List<RegistereAuto> getAllRegistereAuto() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<RegistereAuto> cq = cb.createQuery(RegistereAuto.class);
        Root<RegistereAuto> rootEntry = cq.from(RegistereAuto.class);
        CriteriaQuery<RegistereAuto> all = cq.select(rootEntry);
        TypedQuery<RegistereAuto> allQuery = em.createQuery(all);
        return allQuery.getResultList();
    }
    
}
