/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject;

import cz.fel.cvut.cz.prakingnew.Parkovsite;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Rooben
 */
public class DBManager {

    EntityManagerFactory emf;
    EntityManager em;
    Parkovsite p;

    /**
     * used for inserts
     */
    public DBManager() {
        emf = Persistence.createEntityManagerFactory("PrakingNew");
        em = emf.createEntityManager();
    }

    /**
     * used for update or delete
     */
    public DBManager(Parkovsite p) {
        emf = Persistence.createEntityManagerFactory("PrakingNew");
        em = emf.createEntityManager();

        this.p = p;
    }

    /**
     * clean up all instances of this class
     */
    public void close() {
        em.close();
        emf.close();
    }

    /**
     * insert this record
     */
    public void insert(Parkovsite p) {
        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();
    }

    //find a record by it's id
    public Parkovsite findById(int id) {
        Parkovsite p = em.find(Parkovsite.class, id);

        return p;
    }

//    //delete this record
//    public void delete(Parkovsite p) {
//        em.getTransaction().begin();
//        //this is a little weird as we have to find the object once again
//        //before its deleted - required because the object has to be in
//        //the same context - too complicated to explain fully here
//        Parkovsite park = findById(p.getId());
//        em.remove(park);
//        em.getTransaction().commit();
//    }
//    //update this record
//    public void update (Parkovsite p) {
//        Parkovsite contacts = em.find(Parkovsite.class, p.getId());
//        em.getTransaction().begin();
//        contacts.setFirstname(p.getFirstname());
//        contacts.setLastname(p.getLastname());
//        contacts.setStreet(p.getStreet());
//        contacts.setCity(p.getCity());
//        em.getTransaction().commit();
//    }
}
