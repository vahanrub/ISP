/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fel.cvut.cz.parkingproject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Rooben
 */
@Entity
@Table(name = "Registrovana_vozidla")
public class RegistereAuto {

    @Id
    @Column(name = "spz")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "datum_registrace")
    private String registratedDate;

    @Column(name = "prijmeni")
    private int surname;

    @Column(name = "jmeno")
    private String name;

    @Column(name = "email")
    private int email;

    @Column(name = "mobil")
    private String mobileNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegistratedDate() {
        return registratedDate;
    }

    public void setRegistratedDate(String registratedDate) {
        this.registratedDate = registratedDate;
    }

    public int getSurname() {
        return surname;
    }

    public void setSurname(int surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEmail() {
        return email;
    }

    public void setEmail(int email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        return "Parking{"
                + "id=" + id
                + ", registratedDate=" + registratedDate
                + ", surname=" + surname
                + ", name=" + name
                + ", email=" + email
                + ", mobileNumber=" + mobileNumber
                + '}';
    }

}
